Launch an empty Juptyer environment on Binder by clicking on this button : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fempty-jupyter-binder-repo/master?urlpath=lab%2Ftree)

Please do not commit to this repo.